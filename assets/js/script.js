jQuery(function ($) {
    "use strict";
    $('.main-menu nav ul li').hover(function () {
       $(this).addClass('open').find('> ul.sub-menu').stop(true, true).fadeIn();
    },function () {
        $(this).removeClass('open').find('> ul.sub-menu').stop(true, true).fadeOut();
    });
});